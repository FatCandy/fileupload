/**
 * Created by wallace on 11/04/2017.
 */
var http = require("http");
var https = require("https");
var url = require("url");
var crypto = require("crypto");
var fs = require("fs");
var settings = require('./settings');
var format = settings.format_string;
var allowedDomains = [];
var secureContext = {};
var options = {};

function configure() {
    var domains = settings.domains;
    console.log("File Uploader will accept secure requests for the following domains:");
    for (var domain in domains) {
        console.log("Domain=www." + domain);

        allowedDomains.push("www." + domain);

        if (settings.PRODUCTION) {
            secureContext["www." + domain] = crypto.createCredentials({
                key: fs.readFileSync(domains[domain].key), // (!settings.PRODUCTION ? local_path : server_path) + config.domains[domain].key
                cert: fs.readFileSync(domains[domain].cert), // (!settings.PRODUCTION ? local_path : server_path) + config.domains[domain].cert
                ca: [fs.readFileSync(domains[domain].fullchain)] // /etc/ssl/certs/lets-encrypt-x1-cross-signed.pem
            }).context;
        }
    }

    //provide a SNICallback when you create the options for the https server
    if (settings.PRODUCTION) {
        options = {
            SNICallback: function (domain_name) {
                return secureContext[domain_name];
            },
            cert: fs.readFileSync('certificate.crt'),
            key: fs.readFileSync('key.nopass'),
            ca: [fs.readFileSync('server.ca.pem')]
        };
    }
}

function start(route, handle) {

    function onRequest(request, response) {
        var pathname = url.parse(request.url).pathname;
        var validDomain = false;
        for (var i = 0; i < allowedDomains.length; i++) {
            // console.log(allowedDomains[i]);
            if ((!settings.PRODUCTION ? 'http://' : 'https://') + allowedDomains[i] == request.headers.origin) {
                if (request.method === 'OPTIONS') {
                    var headers = {};
                    // IE8 does not allow domains to be specified, just the *
                    // headers["Access-Control-Allow-Origin"] = req.headers.origin;
                    headers["Access-Control-Allow-Origin"] = (!settings.PRODUCTION ? 'http://' : 'https://') + request.headers['host'].split(":")[0];
                    headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
                    headers["Access-Control-Allow-Credentials"] = 'true';
                    response.writeHead(200, headers);
                    response.end();
                    return;
                } else {
                    route(handle, pathname.substring(0, pathname.indexOf('/', 1)), response, request, pathname.substring(pathname.indexOf('/', 2) + 1, pathname.length));
                    return;
                }
            } else {
                var host = (!settings.PRODUCTION ? 'http://' : 'https://') + request.headers.host.substr(0, request.headers.host.lastIndexOf(":"));
                if ((!settings.PRODUCTION ? 'http://' : 'https://') + allowedDomains[i] == host) {
                    route(handle, pathname.substring(0, pathname.indexOf('/', 1)), response, request, pathname.substring(pathname.indexOf('/', 2) + 1, pathname.length));
                    return;
                }
            }
        }

        response.writeHead(404, {"Content-Type": "text/html"});
        response.write("404 Not found");
        response.end();
    }

    // Setup the server config
    configure();

    if (settings.PRODUCTION) {
        console.log("HTTPS SERVER CREATED");
        https.createServer(options, onRequest).listen(1027);
    } else {
        console.log("HTTP SERVER CREATED");
        http.createServer(onRequest).listen(1027);
    }
    console.log("File Uploader has started.");
}

exports.start = start;
