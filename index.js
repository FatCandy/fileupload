/**
 * Created by wallace on 11/04/2017.
 */

var server = require("./server");
var router = require("./router");
var requestHandler = require("./requestHandler");
var base64 = require("./base64");
var mysql = require('mysql');
var settings = require('./settings');
var connection = mysql.createConnection(
    {
        host: 'localhost',
        user: settings.sql_user,
        password: settings.sql_pass,
        database: settings.sql_db,
    }
);

var handle = {};
handle["/"] = requestHandler.start;
handle["/start"] = requestHandler.start;
handle["/upload"] = requestHandler.upload;

server.PRODUCTION = true;

var domains = new Object();
connection.on('close', function (err) {
    if (err) {
        // Oops! Unexpected connection close, lets reconnect.
        connection = mysql.createConnection(connection.config);
    } else {
        console.log('Connection closed normally.');
    }
});
connection.connect();

var queryString = 'SELECT * FROM domains';

connection.query(queryString, function (err, rows, fields) {
    if (err) throw err;

    for (var i in rows) {
        var key = rows[i].name;
        domains[key] = {cert: rows[i].cert, key: rows[i].ssl_key, name: rows[i].name, fullchain: rows[i].fullchain};
    }
    settings.domains = domains;
    server.start(router.route, handle);
});

connection.end();