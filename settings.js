/**
 * Created by wallace on 11/04/2017.
 */
exports.PRODUCTION = false;
exports.sql_user = "root";
exports.sql_pass = "pass";
exports.sql_db = "some_db";
exports.domains = {};

exports.format_string = function (message) {
    var params = [].slice.call(arguments);
    var result = message;

    if (params.length > 1) {
        for (var i = 1; i < params.length; i++) {
            result = result.replace(new RegExp("\\{" + (i - 1) + "\\}", "g"), params[i]);
        }
    }
    return result;
}