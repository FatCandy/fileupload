/**
 * Created by wallace on 11/04/2017.
 */

function route(handle, pathname, response, request, dir) {
    if (typeof handle[pathname] === 'function') {
        handle[pathname](response, request, dir);
    }
    else {
        console.log("No request handler found for path=" + pathname);
        response.writeHead(404, {"Content-Type": "text/html"});
        response.write("404 Not found");
        response.end();
    }
}

exports.route = route;