/**
 * Created by wallace on 11/04/2017.
 */

var util = require('util'), http = require('http'), path = require('path'), async = require('async'),
    settings = require('./settings'), format = settings.format_string, querystring = require("querystring"),
    fs = require("fs"), formidable = require("formidable");

function get_domain(req) {
    var host = req.headers.origin;
    if (req.headers.origin == undefined) {
        host = "https://" + req.headers.host.substr(0, req.headers.host.lastIndexOf(":"));
    }
    var r = /:\/\/[a-zA-Z]+.([^\/]+)/;
    var domain = host.match(r)[1];
    var r1 = /:\/\/([a-zA-Z]+)\./;
    var sub_domain = host.match(r1)[1];

    if (sub_domain != "www") {
        domain = sub_domain + "." + domain;
    }
    return domain;
}

function upload(response, request, dirFromURL) {

    var form = new formidable.IncomingForm(), files = [], fields = [];

    form.uploadDir = "./tmp";

    form
        .on('field', function (field, value) {

            console.log(field, value);
            fields.push([field, value]);
        })

        .on('file', function (field, file) {

            console.log('Directory=' + dirFromURL);
            dir = dirFromURL;

            // Allow only the following file types to be uploaded
            if (file.type == 'image/png' || file.type == 'image/gif' ||
                file.type == 'image/pjpeg' || file.type == 'image/jpeg' ||
                file.type == 'application/pdf' || file.type == 'application/msword' ||
                file.type == 'application/msword' ||
                file.type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                files.push(file);
            } else {
                console.log('Error! Wrong file type.');
            }
        })

        .on('end', function () {

            var host = request.headers['host'].split(":");

            if (files.length > 0) {
                var domain = get_domain(request);
                // Extract form field values for use in image processing
                var base_path = (!settings.PRODUCTION ?
                    format("/Users/wallace/Documents/output/{0}/app/webroot/files/", domain) :
                    format("/var/www/{0}/app/webroot/files/", domain));
                var new_path = base_path + dir;

                console.log("Using path=" + base_path);
                async.waterfall([

                    // Check if the path exists
                    function (callback) {
                        fs.exists(new_path, function (exists) {
                            if (!exists) {
                                console.log("Path=" + new_path + " does not exist...");
                                callback(null, false);
                            }
                            else {
                                console.log("Path=" + new_path + " exists");
                                callback(null, true);
                            }
                        });
                    },

                    // Create the directory if required
                    function (pathExists, callback) {
                        if (!pathExists) {
                            console.log('Creating directory=' + new_path);
                            fs.mkdir(new_path, '754', function (err) {
                                if (err) {
                                    callback(err);
                                }
                                console.log('Creating path=' + new_path);
                                callback(null, 'done');
                            });
                        }
                        else {
                            callback(null, 'done');
                        }
                    },

                ], function (err, result) {
                    if (err) {
                        console.error('Error=' + err);
                    }
                    else {
                        if (result == 'done') {

                            dir = ((dir == '' || dir == undefined) ? 'noname' : dir);

                            var new_file_name = files[0].name.replace(/\s/g, '');
                            var new_file_path = base_path + dir + '/' + new_file_name;

                            fs.rename(files[0].path, new_file_path,
                                function (err) {
                                    if (err) throw err;
                                    // console.log('The file has been saved!');
                                });

                            response.writeHead(200, {
                                'Access-Control-Allow-Credentials': 'true',
                                'Access-Control-Allow-Origin': 'https://' + request.headers['host'].split(":")[0]
                            });
                        }
                    }
                });
            }
        });
    form.parse(request);
}

process.on('uncaughtException', function (err) {
    console.log(err);
});

exports.upload = upload;